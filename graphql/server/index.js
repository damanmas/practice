const axios = require('axios')
const jwt = require('jsonwebtoken')
const { ApolloServer, UserInputError, gql, PubSub } = require('apollo-server')
const { v1: uuid } = require('uuid')

const pubsub = new PubSub()

const JWT_SECRET = 'FUTUNIO_SECRET_KEY'

let _database = []

const url = 'http://localhost:5000/api'

const typeDefs = gql`
    enum ElectionType {
        eduskuntavaalit
        kunnallisvaalit
        presidentinvaalit
    }
    
    type CandidateEntry {
        CandidateId: Int!
        CandidateName: String!
        CandidateAge: Int
        CandidateVotes: Int!
        CandidateNumber: Int!
        Election_FK: Int!
    }
    
    type ElectionEntry {
        ElectionId: Int!
        ElectionName: String!
        ElectionYear: Int!
        ElectionType: ElectionType!
    }
    
    type ResJSON {
        message: String!
    }

    type Token {
        value: String!
    }
    
    type ElectionWithoutId {
        ElectionName: String!
        ElectionYear: Int!
        ElectionType: ElectionType!
    }
    
    type Query {
        getElections: [ElectionEntry!]!
        getCandidates(ElectionId: Int!): [CandidateEntry!]!
    }
    
    type Mutation {
        setCurrentElection(
            ElectionId: Int!
            ElectionName: String!
        ): Token
        addVote(
            CandidateId: Int!
            CandidateVotes: Int!
        ): ResJSON
        addElection(
            ElectionName: String!
            ElectionYear: Int!
            ElectionType: ElectionType!
        ): ResJSON
        addCandidate(
            CandidateName: String!
            CandidateAge: Int!
            Election_FK: Int!
        ): ResJSON
    }
    
    type Subscription {
        electionAdded: ElectionWithoutId!
    }    
`

const config = {
    mode: 'cors',
    headers: { 'Content-type': 'application/json' }
}

const resolvers = {
    Query: {
        getElections: async () => {
            const res = await axios.get(`${url}/election/all`, { ...config, method: 'GET' })
            return await res.data
        },
        getCandidates: async (root, args) => {
            const res = await axios.get(`${url}/candidate/${args.ElectionId}`,{ ...config, method: 'GET' })
            return await res.data
        }
    },
    Subscription: {
        electionAdded: {
            subscribe: () => pubsub.asyncIterator(['ELECTION_ADDED'])
        }
    },
    Mutation: {
        setCurrentElection: (root, args) => {
            const electionForToken = {
                name: args.ElectionName,
                id: args.ElectionId,
            }
            const token = jwt.sign(electionForToken, JWT_SECRET)
            return { value: token }
        },
        addVote: async (root, args) => {
            try {
                const res = await axios.put(`${url}/candidate/${args.CandidateId}`,
                    { CandidateVotes: args.CandidateVotes + 1 },
                    { ...config, method: 'PUT', /*headers: { ...config.headers, type: 'voted' }*/ }
                )
                const msg = await res.data
                return { message: msg }
            } catch (e) {
                throw new UserInputError('Incorrect parameters', {
                    invalidArgs: e.message,
                })
            }
        },
        addElection: async (root, args) => {
            try {
                console.log(args)
                const res = await axios.post(`${url}/election`,
                    args,
                    { ...config, method: 'POST' }
                )
                await pubsub.publish('ELECTION_ADDED', { electionAdded: args })
                const msg = await res.data
                return { message: msg }
            } catch (e) {
                throw new UserInputError('Incorrect parameters', {
                    invalidArgs: e.message,
                })
            }
        },
        addCandidate: async (root, args) => {
            try {
                const finalItem = {
                    args,
                    CandidateNumber: Math.floor(Math.random() * 1500)
                }
                const res = await axios.post(`${url}/candidate/${args.Election_FK}`,
                    finalItem,
                    { ...config, method: 'POST' }
                )
                const msg = await res.data
                return { message: msg }
            } catch (e) {
                throw new UserInputError('Incorrect parameters', {
                    invalidArgs: e.message,
                })
            }
        }
    }
}

const server = new ApolloServer({
    typeDefs,
    resolvers,
    /*context: async ({ req }) => {
        const auth = req ? req.headers.authorization : null
        if (auth && auth.toLowerCase().startsWith('bearer ')) {
            const decodedToken = jwt.verify(
                auth.substring(7), JWT_SECRET
            )
            const currentUser = await User.findById(decodedToken.id).populate('friends')
            return { currentUser }
        }
    }*/
})

server.listen().then(({ url, subscriptionsUrl }) => {
    console.log(`Server ready at ${url}`)
    console.log(`Subscriptions ready at ${subscriptionsUrl}`)
})
