import './App.css'
import { gql, useQuery } from '@apollo/client'
import Persons from './Persons'
import PersonForm from './PersonForm'
import { ALL_PERSONS } from './queries'
import { useState } from 'react'

const App = () => {
    const [errorMessage, setErrorMessage] = useState(null)

    const result = useQuery(ALL_PERSONS, {
        pollInterval: 2000
    })

    if(result.loading)  {
        return <div>loading...</div>
    }

    const notify = (message) => {
        setErrorMessage(message)
        setTimeout(() => {
            setErrorMessage(null)
        }, 10000)
    }

    return (
        <div className="App">
            <Persons persons={result.data.allPersons} />
            <Notify errorMessage={errorMessage} />
            <PersonForm setError={notify} />
        </div>
    )
}

export default App

const Notify = ({errorMessage}) => {
    if(!errorMessage) {
        return null
    }
    return (
        <div style={{color: 'red'}}>
            {errorMessage}
        </div>
    )
}

