import StateManager from '../statemanager/StateManager'

const Candidate = (props) => {

    const handleVote = async (e) => {
        e.preventDefault()
        props.addVote(props.candidate)
    }

    return (
        <div className="candidate-wrapper">
            { props.candidate ?
                <>
                    <h3>{props.candidate['CandidateName']}</h3>
                    <h6>Ikä: {props.candidate['CandidateAge']}</h6>
                    <section onClick={handleVote}>{props.candidate['CandidateNumber']}</section>
                    <h5>Äänet: {props.candidate['CandidateVotes']}</h5>
                </>
                : null
            }
        </div>
    )
}

export default StateManager(Candidate)
