import { useCallback, useEffect, useState } from 'react'
import { Pie } from 'react-chartjs-2'
import StateManager from '../statemanager/StateManager'

const ElectionChart = (props) => {
    const [presentations, setPresentations] = useState({})
    const [currentElections, setCurrentElections] = useState(null)
    const [currentActive, setCurrentActive] = useState(null)
    const [ctrlButtons, setCtrlButtons] = useState([])
    const { elections } = props.state

    useEffect(() => {
        if(elections.list) setCurrentElections(elections.list)
    }, [elections.list])

    useEffect(() => {
        if(currentElections && currentElections.length > 0) {
            setCurrentActive(currentElections[0]['name'])
            currentElections.forEach((election, i) => {
                const name = election['name']
                let data = {
                    labels: [],
                    datasets: []
                }
                let dataset = {
                    label: 'votes',
                    data: [],
                    backgroundColor: [],
                    borderColor: [],
                    borderWidth: 1,
                }
                election['candidates'].forEach(candidate => {
                    data.labels.push(candidate['name'])
                    dataset.data.push(candidate['votes'])
                    dataset.backgroundColor.push(randomColor(0.2))
                    dataset.borderColor.push(randomColor(1))
                })
                data.datasets.push(dataset)
                if(ctrlButtons.length <= i) {
                    setCtrlButtons(prevState =>
                        [...prevState,
                            (<button key={i}
                                name={name}
                                className="m-2"
                                onClick={() => setCurrentActive(name)}>
                                {name}
                            </button>)
                        ])
                }
                setPresentations(prev => ({
                    ...prev,
                    [election['name']]: data
                }))
            })
        }
    }, [currentElections])

    const renderPieChart = useCallback(() => {
        return Object.keys(presentations).length !== 0 ?
            <Pie data={presentations[currentActive]} options={options} />
            : null
    }, [presentations, currentActive])

    return (
        <div>
            <div className="election-chart-container">
                <div className="centering">
                    <h1 className="title mb-4">Tämänhetkinen äänestystilanne</h1>
                    <h4 className="mb-3">Valitse vaaleista:</h4>
                    <div>{ ctrlButtons ? ctrlButtons : null }</div>
                </div>
                { renderPieChart() }
            </div>
        </div>
    )
}

export default StateManager(ElectionChart)

const randomColor = (opacity) => {
    return 'rgba(' + Math.round(Math.random() * 255) + ',' + Math.round(Math.random() * 255) + ',' + Math.round(Math.random() * 255) + ',' + (opacity || '.3') + ')'
}

const options = {
    responsive: true,
    layout: {
        padding: {
            left: 1,
            right: 5,
            top: 1,
            bottom: 1,
        }
    },
    legend: {
        display: true,
        position: 'top',
        labels: {
            fontSize: 14,
        }
    },
    tooltips: {
        mode: 'index',
        intersect: false,
        position: 'nearest',
    }
}