import { useEffect, useState } from 'react'
//import { Row, Modal } from 'react-bootstrap'
import { Button } from '@material-ui/core'
import { useFormikContext, Formik, Form } from 'formik'
import { ElectionModalContent, CandidateModalContent } from './ModalContent'
import StateManager from '../../statemanager/StateManager'

const ModalWrapper = (props) => {
    const [showState, setShowState] = useState(false)
    const [initialValues, setInitialValues] = useState(null)
    const [translation, setTranslation] = useState('')

    useEffect(() => {
        setInitialValues(props.current === 'election' ?
            { electionName: '', electionYear: '', electionType: '' } : { candidateName: '', candidateAge: '' }
        )
        setTranslation(props.current === 'election' ? 'vaali' : 'ehdokas')
    }, [props])

    useEffect(() => {
        setShowState(props.show)
    }, [props.show])

    const addItem = async (values) => {
        await props.addNewItem(props.current, values)
    }

    /*const addItem = async (values) => {
        if(window.confirm(`Haluatko varmasti listätä ${values.name}?`)) {
            try {
                const res = await axios.post(`${process.env.REACT_APP_SERVER_URL}/api/${props.current}${props.electionid}`,
                    values,
                    {
                        method: 'POST',
                        mode: 'cors',
                        headers: {
                            'Content-type': 'application/json'
                        }
                    })
                const resolved = await res.data
                if(resolved) props.addItemToArr(`${props.current}s`, 'list', resolved)
            } catch (e) {
                console.log('addNewItem error:',e)
            }
            props.onHide()
        }
    }*/

    return (
        <div
            /*show={showState}
            onHide={props.onHide}
            backdrop="static"
            keyboard={false}
            size="md"
            centered*/
            className="election-modal-container"
        >
            <div /*closeButton*/>
                <h3 className="m-lg-2">{`Lisää uusi ${translation}`}</h3>
            </div>
            <div>
                <Formik
                    initialValues={initialValues}
                    onSubmit={(values) => {
                        addItem(values)
                    }}>
                    <Form>
                        <div>
                            <div className="form-view">
                                { props.current && props.current === 'election' ?
                                    <ElectionModalContent />
                                    :
                                    <CandidateModalContent />
                                }
                            </div>
                        </div>

                        <GatherValuesAndSubmit onHide={props.onHide} translation={translation} />
                    </Form>
                </Formik>
            </div>
        </div>
    )
}

export default StateManager(ModalWrapper)

const GatherValuesAndSubmit = (props) => {
    const { values, submitForm } = useFormikContext()

    const handleSubmit = (e) => {
        e.preventDefault()
        submitForm().catch(e => console.log(e.message))
    }

    return (
        <div>
            <button
                className="modal-close-button"
                onClick={props.onHide}
            >
                Sulje ikkuna
            </button>
            <button
                className="modal-add-button"
                onClick={handleSubmit}
                type="submit"
            >
                {`Lisää ${props.translation}`}
            </button>
        </div>
    )
}

