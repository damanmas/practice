import { gql } from '@apollo/client'

const CANDIDATE_DETAILS = gql`
    fragment CandidateDetails on CandidateEntry {
        CandidateId
        CandidateName
        CandidateAge
        CandidateVotes
        CandidateNumber
        Election_FK
    }
`

const ELECTION_DETAILS = gql`
    fragment ElectionDetails on ElectionEntry {
        ElectionName
        ElectionYear
        ElectionType
    }
`

export const ALL_ELECTIONS = gql`
    query {
        getElections {
            ElectionId
            ElectionName
            ElectionYear
            ElectionType
        }
    }
`

export const ALL_CANDIDATES = gql`
    query getCandidates($electionId: Int!) {
        getCandidates(ElectionId: $electionId) {
            ...CandidateDetails
        }
    }
    ${CANDIDATE_DETAILS}
`

export const CURRENT_ELECTION = gql`
    mutation setCurrentElectionHandler($electionId: Int!, $electionName: String!) {
        setCurrentElection(ElectionId: $electionId, ElectionName: $electionName) {
            value
        }
    }
`

export const ADD_VOTE = gql`
    mutation addVoteHandler($candidateId: String!, $candidateVotes: Int!) {
        addVote(CandidateId: $candidateId, CandidateVotes: $candidateVotes) {
            message
        }
    }
`

export const PERSON_ADDED = gql`  
    subscription {
        electionAdded {
            ...PersonDetails
        }
    }
    ${ELECTION_DETAILS}
`
export const ADD_ELECTION = gql`
    mutation addElectionHandler($electionName: String!, $electionYear: Int!, $electionType: String!) {
        addElection(ElectionName: $electionName, ElectionYear: $electionYear, ElectionType: $electionType)  {
            message
        }
    }
`

export const ADD_CANDIDATE = gql`
    mutation addCandidateHandler($candidateName: String!, $candidateAge: Int!, $electionId: Int!) {
        addCandidate(CandidateName: $candidateName, CandidateAge: $candidateAge, Election_FK: $electionId)  {
            message
        }
    }
`
