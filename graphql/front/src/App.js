import Navigation from './components/Navigation'
import Routes from './components/Routes'

const App = () => {
    return (
        <div className="grid gap-5 grid-rows-6 grid-cols-1 p-5 min-h-screen font-sans text-xl box-border overflow-auto">
            <header id="header" className="bg-gray-100 row-span-1 flex justify-center">
                <Navigation />
            </header>
            <article id="content" className="bg-gray-100 row-span-4 flex justify-center items-start pt-10 text-center">
                <Routes />
            </article>
            <footer id="footer" className="bg-gray-100 row-span-1">
            </footer>
        </div>
    )
}

export default App
