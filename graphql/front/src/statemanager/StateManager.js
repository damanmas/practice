import React from 'react'
import StateContext from './StateContext'

const StateManager = (Component) => {
	const HOC = (props) => {
		return (
			<StateContext.Consumer>
				{ state => <Component {...props} {...state} /> }
			</StateContext.Consumer>
		)
	}
	return HOC
}

export default StateManager
