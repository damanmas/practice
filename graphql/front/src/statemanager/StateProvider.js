import React, { useEffect, useState } from 'react'
import StateContext from './StateContext'
import { useLocation } from 'react-router-dom'
//import axios from 'axios'
//axios.defaults.withCredentials = true
import { useLazyQuery, useQuery, useMutation, useSubscription } from '@apollo/client'
import {
	ALL_ELECTIONS,
	ALL_CANDIDATES,
	ADD_VOTE,
	ADD_ELECTION,
	ADD_CANDIDATE,
	CURRENT_ELECTION,
	PERSON_ADDED
} from '../Queries'

const defaultState = {
	elections: {
		list: [],
		current: null,
	},
	candidates: {
		list: []
	}
}

export default function StateProvider(props) {
	const [state, setState] = useState(defaultState)
	const { query } = useLocation()
	const elections = useQuery(ALL_ELECTIONS)
	const [ getCandidates, candidates ] = useLazyQuery(ALL_CANDIDATES)
	const [ setCurrentElection ] = useMutation(CURRENT_ELECTION)
	const [ addCandidate ] = useMutation(ADD_CANDIDATE)
	const [ addVoteHandler ] = useMutation(ADD_VOTE)
	const [ addElection ] = useMutation(ADD_ELECTION, {
		onError: (error) => {
			console.log(error.graphQLErrors[0].message)
		},
		/*update: (store, response) => {
			const dataInStore = store.readQuery({ query: ALL_PERSONS })
			store.writeQuery({
				query: ALL_PERSONS,
				data: {
					...dataInStore,
					allPersons: [ ...dataInStore.allPersons, response.data.addPerson ]
				}
			})
		}*/
	})

	useSubscription(PERSON_ADDED, {
		onSubscriptionData: ({ subscriptionData }) => {
			const addedElection = subscriptionData.data.electionAdded
			updateCacheWith(addedElection)
		}
	})

	const updateCacheWith = (addedElection) => {
		const includedIn = (set, object) =>
			set.map(p => p.id).includes(object.id)

		/*const dataInStore = client.readQuery({ query: ALL_ELECTIONS })
		if (!includedIn(dataInStore.allPersons, addedPerson)) {
			client.writeQuery({
				query: ALL_PERSONS,
				data: { allPersons : dataInStore.allPersons.concat(addedPerson) }
			})
		}*/
	}

	const addToStateProvider = (parent, child, data) => {
		setState(prev => ({
			...prev,
			[parent]: {
				...prev[parent],
				[child]: data
			}
		}))
	}

	useEffect(() => {
		if(typeof window !== 'undefined') {
			if(localStorage.getItem('elections')) {
				setState(JSON.parse(localStorage.getItem('elections')))
			}
		}
	}, [])

	useEffect(() => {
		if(typeof window !== 'undefined') {
			localStorage.setItem('elections', JSON.stringify(state))
		}
	}, [state])

	useEffect(() => {
		if(query) {
			const current = state.elections.list.filter(e => query === e.ElectionId)[0]
			addToStateProvider('elections', 'current', current)
			//setCurrentElection({ variables: { electionId: current['ElectionId'], electionName: current['ElectionName'] } })
		}
	}, [query])

	useEffect(() => {
		if(state.elections.current) {
			getCandidates({ variables: { electionId: state.elections.current['ElectionId'] } })
		}
	}, [state.elections.current])

	useEffect(() => {
		if(!elections.loading && elections.data) {
			addToStateProvider('elections', 'list', elections.data.getElections)
		}
	}, [elections])

	useEffect(() => {
		if(candidates.data) {
			addToStateProvider('candidates', 'list', candidates.data.getCandidates)
		}
	}, [candidates])

	const addVote = async (candidate) => {
		console.log(candidate)
		if(window.confirm(`Haluatko varmasti äänestää ${candidate['CandidateName']}? Äänestys mahdollista kerran vaalia kohden.`)) {
			await addVoteHandler({ variables: { candidateId: candidate['CandidateId'], candidateVotes: candidate['CandidateVotes'] } })
		}
	}

	const addNewItem = async (current, values) => {
		const isElection = current === 'election'
		if(window.confirm(`Haluatko varmasti lisätä ${isElection ? values.electionName : values.candidateName}?`)) {
			if(isElection) {
				await addElection({ variables: values })
			} else {
				await addCandidate({ variables: { values: values, Election_FK: state.elections.current.ElectionId } })
			}
		}
	}

	const resetMemory = (e) => {
		e.preventDefault()
		if(window.confirm('Nollaatko selainmuistin?')) {
			if(localStorage.getItem('elections')) {
				localStorage.setItem('elections', JSON.stringify(defaultState))
			}
		}
	}

	return(
		<StateContext.Provider value={{
			state,
			addNewItem,
			addVote,
			resetMemory
			//getElections: getElections,
			//getCandidates: getCandidatesHandler,
			//setElection: setElection,
			//addItemToArr: addItemToArr,
		}}>
			{props.children}
		</StateContext.Provider>
	)
}


/*const addItemToArr = (parent, child, data) => {
    setState(prev => ({
        ...prev,
        [parent]: {
            ...prev[parent],
            [child]: prev[parent][child].concat(data)
        }
    }))
}*/

/*const getElections = async () => {
    try {
        const res = await axios.get(`{process.env.REACT_APP_SERVER_URL}/api/election/all`,
            {
                method: 'GET',
                mode: 'cors',
                headers: {
                    'Content-type': 'application/json'
                }
            })
        const resolved = await res.data
        addToStateProvider('elections', 'list', resolved)
    } catch (e) {
        console.log('getElections error:',e)
    }
}

const getCandidates = async (id) => {
    try {
        const res = await axios.get(`{process.env.REACT_APP_SERVER_URL}/api/candidate/{id}`,
            {
                method: 'GET',
                mode: 'cors',
                headers: { 'Content-type': 'application/json' }
            })
        const resolved = await res.data
        addToStateProvider('candidates', 'list', resolved)
    } catch (e) {
        console.log('getCandidates error:',e)
    }
}

const addVote = async (candidate) => {
    if(window.confirm(`Haluatko varmasti äänestää {candidate['CandidateName']}? Äänestys mahdollista kerran vaalia kohden.`)) {
        try {
            let modified = {
                ...candidate,
                votes: candidate.votes + 1
            }
            const res = await axios.put(
                `{process.env.REACT_APP_SERVER_URL}/api/candidate/{candidate['CandidateId']}`,
                modified,
                {
                    method: 'PUT',
                    mode: 'cors',
                    headers: {
                        'Content-type': 'application/json',
                        'type': 'voted'
                    }
                })

            if(res.status === 200) {
                const resolved = await res.data
                //const found = state.elections.list.filter(item => query === item._id)
                const updatedList = state.candidates.list.map(candidate => {
                    return candidate._id === resolved._id ? resolved : candidate
                })
                addToStateProvider('candidates', 'list', updatedList)
                getElections().catch(e => console.log(e.message))
            }
        } catch (e) {
            if(e.response.status === 403) {
                alert('Olet jo äänestänyt näissä vaaleissa.')
            } else {
                console.log('getElection error:',e)
            }
        }
    }
}*/

