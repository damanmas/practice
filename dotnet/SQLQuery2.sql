create table dbo.Candidate(
 CandidateId int identity(1,1) primary key,
 CandidateName varchar(150),
 CandidateAge int,
 CandidateNumber int,
 CandidateVotes int,
 Election_FK int foreign key references Election(ElectionId)
)

insert into dbo.Candidate values(
 'Bar', 31, 112, 0, 1
)

select * from dbo.Candidate

select * from dbo.Candidate where Election_FK = 2