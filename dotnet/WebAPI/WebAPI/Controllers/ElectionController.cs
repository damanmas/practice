﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ElectionController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public ElectionController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /*[HttpGet]
        public JsonResult Get()
        {
            string query = @"select * from dbo.Election";

            DataTable table = new DataTable();

            string sqlDataSource = _configuration.GetConnectionString("FutunioAppConn");

            SqlDataReader myReader;
            using(SqlConnection myCon=new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using(SqlCommand myCommand=new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
              
            }

            return new JsonResult(table);
        }*/

        [HttpGet("{param}")]
        public JsonResult Get(string param)
        {
            string added = param == "all" ? "" : "where ElectionId = '" + param + "'";
            string query = @"select * from dbo.Election " + added;

            DataTable table = new DataTable();

            string sqlDataSource = _configuration.GetConnectionString("FutunioAppConn");

            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }

            }

            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(Election election)
        {
            string query = @"insert into dbo.Election values ('"+election.ElectionName+"','"+election.ElectionYear+"','"+election.ElectionType+"')";

            DataTable table = new DataTable();

            string sqlDataSource = _configuration.GetConnectionString("FutunioAppConn");

            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }

            }

            return new JsonResult("Added successfully");
        }

        [HttpPut]
        public JsonResult Put(Election election)
        {
            string query = @"update dbo.Election set ElectionName = '"+election.ElectionName+"' where ElectionId = '"+election.ElectionId+"'";

            DataTable table = new DataTable();

            string sqlDataSource = _configuration.GetConnectionString("FutunioAppConn");

            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }

            }

            return new JsonResult("Updated name successfully");
        }

    }
}
