﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CandidateController : ControllerBase
    {

        private readonly IConfiguration _configuration;

        public CandidateController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet("{param}")]
        public JsonResult Get(string param)
        {
            string parsed = param == "all" ? "" : "where Election_FK = '" + param + "'";
            string query = @"select * from dbo.Candidate " + parsed;

            DataTable table = new DataTable();

            string sqlDataSource = _configuration.GetConnectionString("FutunioAppConn");

            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }

            }

            return new JsonResult(table);
        }

        [HttpPost("{id}")]
        public JsonResult Post(int id, Candidate candidate)
        {

            string query = @"insert into dbo.Candidate values 
                           ('" + candidate.CandidateName + "','" + candidate.CandidateAge + "','" + candidate.CandidateNumber + "','"+ 0 +"','"+ id +"')";

            DataTable table = new DataTable();

            string sqlDataSource = _configuration.GetConnectionString("FutunioAppConn");

            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }

            }

            return new JsonResult("Added successfully");
        }

        [HttpPut("{id}")]
        public JsonResult Put(int id, Candidate candidate)
        {
            string query = @"update dbo.Candidate set CandidateVotes = '" + Convert.ToInt32(candidate.CandidateVotes) + "' where CandidateId = '" + id + "'";

            DataTable table = new DataTable();

            string sqlDataSource = _configuration.GetConnectionString("FutunioAppConn");

            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }

            }

            return new JsonResult("Updated candidate votes successfully");
        }

    }
}
