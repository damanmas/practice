﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Candidate
    {
        public int CandidateId { get; set; }

        public string CandidateName { get; set; }

        public int CandidateAge { get; set; }

        public int CandidateNumber { get; set; }

        public int CandidateVotes { get; set; }

        public Election election { get; set; }

    }
}
