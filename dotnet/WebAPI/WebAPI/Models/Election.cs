﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Election
    {
        public int ElectionId { get; set; }

        public string ElectionName { get; set; }

        public string ElectionYear { get; set; }

        public string ElectionType { get; set; }

    }
}
