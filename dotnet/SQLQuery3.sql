create table dbo.Election(
 ElectionId int identity(1,1) primary key,
 ElectionName varchar(500),
 ElectionYear int,
 ElectionType varchar(500)
)

select * from dbo.Election

insert into dbo.Election values 
('NEW TEST', 2024, 'kunnallisvaalit')