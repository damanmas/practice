import React, { useRef, useEffect } from 'react'
import EmailEditor from 'react-email-editor'
//import JHL from './designs/JHLFeeRefMemLet.html'
//import { renderToString } from 'react-dom/server'
//import mjml2html from 'mjml'

const Viewer = () => {
    return <div>I am a custom tool.</div>
}

//const template = { __html: JHL }
/*const item = React.createElement({
    render: function() {
        return(
            <div dangerouslySetInnerHTML={template} />
        );
    }
})*/

function App() {
    const emailEditorRef = useRef(null)

    const exportHtml = () => {
        emailEditorRef.current.editor.exportHtml((data) => {
            const { design, html } = data
            console.log('exportHtml', html)
            console.log('exportDesign', design)
        })
    }

    const onLoad = () => {
        // const templateJson = {};
        // emailEditorRef.current.editor.loadDesign(templateJson);
        /*const reader = new FileReader()
        reader.onload = (e) => {
            console.log(e.target.result)
        }
        reader.readAsText('./designs/JHLFeeRefMemLet.html')*/

        const xhttp = new XMLHttpRequest()
        xhttp.onreadystatechange = function() {
            if(this.readyState === 4 && this.status === 200) {
                const parser = new DOMParser();
                const xmlDoc = parser.parseFromString(this.responseText, 'text/xml')
                console.log(xmlDoc)
                const html = xmlDoc.getElementsByTagName('html')
                console.log(html)
                test(html[0])
            }
        }
        xhttp.open('GET', 'src/designs/JHLFeeRefMemLet.html', false)
        xhttp.send(null)
    }

    const test = (html) => {
        emailEditorRef.current.editor.loadDesign({
            html: html,
            classic: true,
        })
    }

    /*console.log(window.location.protocol + '//' + window.location.host + '/custom.js')

    const customTool = {
        name: 'my_tool',
        label: 'My Tool',
        icon: 'fa-smile',
        supportedDisplayModes: ['web', 'email'],
        options: {},
        values: {},
        renderer: {
            Viewer: Viewer, // our React Viewer
            exporters: {
                web: function(values) {
                    return "<div>I am a custom tool.</div>"
                },
                email: function(values) {
                    return "<div>I am a custom tool.</div>"
                }
            },
            head: {
                css: function(values) {},
                js: function(values) {}
            }
        }
    }*/

    return (
        <div className="App">
            <div>
                <button onClick={exportHtml}>Export HTML</button>
            </div>

            <EmailEditor
                ref={emailEditorRef}
                onLoad={onLoad}
                style={{}}
                minHeight={'100vh'}
                options={{
                    /*customJS: [
                        window.location.protocol + '//' + window.location.host + '/custom.js',
                    ]*/
                }}
                tools={{}}
                appearance={{
                    theme: 'dark',
                    panels: {
                        tools: {
                            dock: 'right'
                        }
                    }
                }}
                projectId=""
            />
        </div>
    )
}

export default App

const testhtml = '<body>\n' +
    '\n' +
    '\n' +
    '\n' +
    '  <div class="first-page-wrapper">\n' +
    '    <!-- start: first page header -->\n' +
    '    @phr:jhl.letter.header@\n' +
    '    <!-- end: first page header -->\n' +
    '\n' +
    '    <!-- start: content -->\n' +
    '    <section class="first-page-content">\n' +
    '      <section class="page-content-container">\n' +
    '        <p><b>Hyvä @recipientname@</b></p>\n' +
    '        @cond:membertype=L@\n' +
    '        <p>Maksa jäsenmaksusi kuukausittain alla olevien maksutietojen mukaan.</p>\n' +
    '        <p>Liiton jäsenmaksua maksetaan kaikesta ennakonpidätyksen alaisesta ansiotulosta ennen veroja. Jäsenmaksun laskukaava: bruttopalkka * @field:memberfeework@/100. Jäsenmaksun eräpäivä on aina seuraavan kuukauden @field:paymentdueday@. päivä.</p>\n' +
    '        <p>Huomioithan että jäsenyytesi ei sisällä työttömyyskassan jäsenyyttä. Vain työttömyyskassan jäsenenä voit kerryttää työssäoloehtoa, jonka täytyttyä olet oikeutettu hakemaan työttömyyskassan maksamia etuuksia. Työttömyyskassaan voit liittyä, kun sinulla on voimassaoleva työsuhde.</p>\n' +
    '        <p>Ilmoitathan liiton jäsenpalveluun, mikäli haluat liittyä myös työttömyyskassaan ja alkaa kerryttää ansiosidonnaista työttömyysturvaa.</p>\n' +
    '        @cend:membertype=L@\n' +
    '        @cond:membertype=AL@\n' +
    '        <p>Maksa jäsenmaksusi kuukausittain alla olevien maksutietojen mukaan.</p>\n' +
    '        <p>Huomioithan että jäsenyytesi ei sisällä työttömyyskassan jäsenyyttä. Vain työttömyyskassan jäsenenä voit kerryttää työssäoloehtoa, jonka täytyttyä olet oikeutettu hakemaan työttömyyskassan maksamia etuuksia. Työttömyyskassaan voit liittyä, kun yritystoimintasi on päättynyt ja sinulla on voimassa oleva työsuhde JHL:n järjestäytymisalalla.</p>\n' +
    '        <p>Yrittäjien työttömyysturvasta voit tiedustella SYT:stä tai AYT:stä.</p>\n' +
    '        @cend:membertype=AL@\n' +
    '        @cond:membertype=ML@\n' +
    '        <p>Maksa jäsenmaksusi neljännesvuosittain alla olevien maksutietojen mukaan.</p>\n' +
    '        <p>Huomioithan että jäsenyytesi ei sisällä työttömyyskassan jäsenyyttä. Vain työttömyyskassan jäsenenä voit kerryttää työssäoloehtoa, jonka täytyttyä olet oikeutettu hakemaan työttömyyskassan maksamia etuuksia. Työttömyyskassaan voit liittyä, kun sinulla on voimassa oleva työsuhde JHL:n järjestäytymisalalla.</p>\n' +
    '        @cend:membertype=ML@\n' +
    '        @cond:membertype=MJ,VT@\n' +
    '        <p>Maksa jäsenmaksusi neljännesvuosittain alla olevien maksutietojen mukaan.</p>\n' +
    '        @cend:membertype=MJ,VT@\n' +
    '        @cond:membertype=J@\n' +
    '        <p>Maksa jäsenmaksusi kuukausittain alla olevien maksutietojen mukaan.</p>\n' +
    '        <p>Liiton ja työttömyyskassan jäsenmaksua maksetaan kaikesta ennakonpidätyksen alaisesta ansiotulosta ennen veroja. Jäsenmaksun laskukaava: bruttopalkka * @field:memberfeework@/100.</p>\n' +
    '        @cend:membertype=J@\n' +
    '        @cond:membertype=OT@\n' +
    '        <p>Päätoimisena opiskelijana olet vapautettu liiton jäsenmaksuista.</p>\n' +
    '        <p>Tarkistathan että JHL:n tulkinnan mukaiset päätoimisen opiskelun kriteerit täyttyvät kohdallasi:</p>\n' +
    '        <ul>\n' +
    '          <li>et ole oppisopimus- tai monimuoto-opiskelija</li>\n' +
    '          <li>et ole normaalissa päivätyössä</li>\n' +
    '          <li>olet pääsääntöisesti päivät koulussa</li>\n' +
    '          <li>olet oikeutettu nostamaan opintotukea</li>\n' +
    '        </ul>\n' +
    '        <p>Opiskeluaikana tehdystä työstä maksat vain työttömyyskassan jäsenmaksua. Jäsenmaksua maksetaan kaikesta ennakonpidätyksen alaisesta ansiotuloista ennen veroja. Työttömyyskassan jäsenmaksu maksetaan aina itse.</p>\n' +
    '        <p>Vapautus liiton jäsenmaksusta päättyy ilmoittamasi opiskeluajan päättyessä. Tämän jälkeen jäsenyytesi muuttuu automaattisesti varsinaiseksi jäsenyydeksi. Varsinaisena jäsenenä maksat liiton ja työttömyyskassan kokonaisjäsenmaksua. Pidäthän huolta jäsenyydestäsi myös opiskelujesi päätyttyä; maksamalla jäsenmaksun itse, valtuuttamalla työnantajan perimään jäsenmaksun suoraan palkastasi, tai ilmoittamalla uudesta jäsenmaksuvapautuksesta.</p>\n' +
    '        @cend:membertype=OT@\n' +
    '        @cond:membertype=Y@\n' +
    '        <p>Maksa jäsenmaksusi kuukausittain alla olevien maksutietojen mukaan.</p>\n' +
    '        <p>Huomioithan että yrittäjänä ollessasi sinulla ei kuitenkaan ole oikeutta palkansaajakassan maksamaan ansiopäivärahaan. Yrittäjänä tehty työ ei myöskään kerrytä palkansaajan työssäoloehtoa.</p>\n' +
    '        <p>Kassan jäsenenä sinulla on kuitenkin ns. jälkisuojaoikeus, jonka tarkoituksena on helpottaa siirtymistä palkansaajasta yrittäjäksi.</p>\n' +
    '        <p>Jälkisuoja tarkoittaa että jos olet täyttänyt palkansaajan työssäoloehdon ennen yrittäjäksi ryhtymistä, säilytät oikeutesi palkansaajan ansiopäivärahaan 18 kuukauden ajan yritystoiminnan aloittamisesta. Jälkisuojasi on voimassa myös siinä tapauksessa, että siirryt yrittäjäkassan jäseneksi.</p>\n' +
    '        <p>Lisätietoja https://tyottomyyskassa.jhl.fi</p>\n' +
    '        <p>Yrittäjien työttömyysturvasta voit tiedustella SYT:stä tai AYT:stä.</p>\n' +
    '        @cend:membertype=Y@\n' +
    '        @cond:isretiree@\n' +
    '        <p>Eläkeläisjäsenenä maksat minimijäsenmaksua joka on @field:memberfeenonwork@ euroa kuukaudessa. Ole hyvä ja maksa jäsenmaksusi neljännesvuosittain (3 * @field:memberfeenonwork@).</p>\n' +
    '\n' +
    '        <b>Maksutiedot:</b><br>\n' +
    '        <table class="bankaccounttable">\n' +
    '          <tr class="bankaccounttablerow">\n' +
    '            <td class="bankaccounttablebank">Maksun saaja:</td>\n' +
    '            <td class="bankaccounttableiban" colspan=2>Julkisten ja hyvinvointialojen liitto JHL ry</td>\n' +
    '          </tr>\n' +
    '          <tr class="bankaccounttablerow">\n' +
    '            <td class="bankaccounttablebank">Maksun määrä:</td>\n' +
    '            <td class="bankaccounttableiban" colspan=2>3 * @field:memberfeenonwork@ euroa</td>\n' +
    '          </tr>\n' +
    '        </table>\n' +
    '        <table>@field:bankaccounttable@</table>  \n' +
    '        <table class="bankaccounttable">\n' +
    '          <tr class="bankaccounttablerow">\n' +
    '            <td class="bankaccounttablebank">Eräpäivä:</td>\n' +
    '            <td class="bankaccounttableiban" colspan=2>Kuukauden @field:paymentdueday@. päivä</td>\n' +
    '          </tr>\n' +
    '        </table>\n' +
    '        <p><b>Käytä aina maksaessasi viitenumeroa @field:reference@</b></p>\n' +
    '        <p>Ilmoitathan kirjallisesti liiton jäsenpalveluun, mikäli et enää halua jatkaa jäsenyyttäsi liitossa.</p>\n' +
    '        @cend:isretiree@\n' +
    '        @cond:!isretiree@\n' +
    '        <b>Maksutiedot:</b><br>\n' +
    '        <table class="bankaccounttable">\n' +
    '          <tr class="bankaccounttablerow">\n' +
    '            <td class="bankaccounttablebank">Maksun saaja:</td>\n' +
    '            <td class="bankaccounttableiban" colspan=2>Julkisten ja hyvinvointialojen liitto JHL ry</td>\n' +
    '          </tr>\n' +
    '          <tr class="bankaccounttablerow">\n' +
    '            <td class="bankaccounttablebank">Maksun määrä:</td>\n' +
    '            <td class="bankaccounttableiban" colspan=2>@cond:memberfeeispercent@@field:memberfeework@% ansiotulosta@cend:memberfeeispercent@@cond:!memberfeeispercent@@field:memberfeenonwork@ euroa/kk @cend:!memberfeeispercent@</td>\n' +
    '          </tr>\n' +
    '        </table>\n' +
    '        <table>@field:bankaccounttable@</table> \n' +
    '        <table class="bankaccounttable">\n' +
    '          <tr class="bankaccounttablerow">\n' +
    '            <td class="bankaccounttablebank">Eräpäivä:</td>\n' +
    '            <td class="bankaccounttableiban" colspan=2>Kuukauden @field:paymentdueday@. päivä</td>\n' +
    '          </tr>\n' +
    '        </table>\n' +
    '        <p><b>Käytä aina maksaessasi viitenumeroa @field:reference@</b></p>\n' +
    '        @cend:!isretiree@\n' +
    '        <p>Pyydämme myös varmistamaan, että jäsen@cond:membertype=OT@- sekä opiskelu@cend:membertype=OT@tietosi ovat ajan tasalla. Voit tarkastaa ja päivittää tietojasi sekä maksaa jäsenmaksusi omaJHL -palvelussa @oopperaurl@.</p>\n' +
    '        <p>Lisätietoja JHL:n jäsenmaksuista: www.jhl.fi/jasenmaksu.</p>\n' +
    '      </section>\n' +
    '      <section class="page-content-signature">\n' +
    '        @phr:jhl.signature.memberregister@\n' +
    '      </section>\n' +
    '    </section>\n' +
    '    <!-- end: content -->\n' +
    '  </div>\n' +
    '\n' +
    '\n' +
    '\n' +
    '  <div class="page-wrapper">\n' +
    '    <section class="page-content">\n' +
    '    <!-- Start: JHL FeeGuide attachment -->\n' +
    '    @phr:jhl.feeguide.attachment@\n' +
    '    <!-- End: JHL FeeGuide attachment -->\n' +
    '    </section>\n' +
    '  </div>\n' +
    '\n' +
    '\n' +
    '</body>'


