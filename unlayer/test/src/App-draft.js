import React, { useRef, useEffect } from 'react'
import { Editor, EditorState } from 'draft-js'
import 'draft-js/dist/Draft.css'

function App() {
    const [editorState, setEditorState] = React.useState(
        () => EditorState.createEmpty(),
    )

    const editor = React.useRef(null)
    function focusEditor() {
        editor.current.focus()
    }

    return (
        <div className="App">
            <Editor
                ref={editor}
                editorState={editorState}
                onChange={setEditorState}
                placeholder="Write something!"
            />
        </div>
    )
}

export default App
