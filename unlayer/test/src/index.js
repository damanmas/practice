import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './App.css';
import App from './App-unlayer';
//import App from './App-draft'

ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById('root')
)
