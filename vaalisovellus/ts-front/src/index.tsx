import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import App from './App'
import StateProvider from './components/statemanager/StateProvider'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './styles/index.css'
import './styles/styles.css'
import * as dotenv from 'dotenv'
dotenv.config()

ReactDOM.render(
    <React.StrictMode>
        <BrowserRouter>
            <StateProvider>
                <App />
            </StateProvider>
        </BrowserRouter>
    </React.StrictMode>,
    document.getElementById('root')
)
