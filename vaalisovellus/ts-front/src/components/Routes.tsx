import React, { ComponentType } from 'react'
import { Route, Switch } from 'react-router-dom'
import Election from './Election'
import Home from './Home'

const Routes: React.FC = () => {
    return (
        <Switch>
            <Route exact path="/" component={() => {
                return <Home />
            }}/>
            <Route exact path="/election/:name" component={Election as ComponentType} />
        </Switch>
    )
}

export default Routes
