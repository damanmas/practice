import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { Row, Modal } from 'react-bootstrap'
import { Button } from '@material-ui/core'
import { useFormikContext, Formik, Form } from 'formik'
import { ElectionModalContent, CandidateModalContent } from './ModalContent'
import StateManager from '../statemanager/StateManager'

interface ModalWrapperProps extends ContextType {
    current: string;
    show: boolean;
    onHide: () => void
    electionid: string;
}

type NameInitials = {
    name: string
}

type ElectionInitials = NameInitials & {
    year: number;
    type: ElectionType;
}

type CandidateInitials = NameInitials & {
    age: number;
}

type Initials = ElectionInitials | CandidateInitials | {}

const ModalWrapper: React.FC<ModalWrapperProps> = (props) => {
    const [showState, setShowState] = useState(false)
    const [initialValues, setInitialValues] = useState<Initials>({})
    const [translation, setTranslation] = useState('')

    useEffect(() => {
        setInitialValues(props.current === 'election' ?
            { name: '', year: '', type: '' } : { name: '', age: '' }
        )
        setTranslation(props.current === 'election' ? 'vaali' : 'ehdokas')
    }, [props.current])

    useEffect(() => {
        setShowState(props.show)
    }, [props.show])

    const addNewItem = async (values: Initials) => {
        if(window.confirm(`Haluatko varmasti listätä ${(values as any).name}?`)) {
            try {
                const res = await axios.post(`${process.env.REACT_APP_SERVER_URL}/api/${props.current}${props.electionid}`, values,
                    {
                        method: 'POST',
                        mode: 'cors',
                        headers: {
                            'Content-type': 'application/json'
                        }
                    })
                const resolved = await res.data
                if(resolved) props.addItemToArr(`${props.current}s`, 'list', resolved)
            } catch (e) {
                console.log('addNewItem error:',e)
            }
            props.onHide()
        }
    }

    return (
        <Modal
            show={showState}
            onHide={props.onHide}
            backdrop="static"
            keyboard={false}
            size="lg"
            centered
            className="election-modal-container"
        >
            <Modal.Header closeButton>
                <h3 className="m-lg-2">{`Lisää uusi ${translation}`}</h3>
            </Modal.Header>
            <Modal.Body>
                <Formik
                    initialValues={initialValues}
                    onSubmit={(values: Initials) => {
                        addNewItem(values).catch(e => console.log(e.message))
                    }}>
                    <Form>
                        <Row lg={12}>
                            <div className="form-view">
                                { props.current && props.current === 'election' ?
                                    <ElectionModalContent />
                                    :
                                    <CandidateModalContent />
                                }
                            </div>
                        </Row>

                        <GatherValuesAndSubmit onHide={props.onHide} translation={translation} />
                    </Form>
                </Formik>
            </Modal.Body>
        </Modal>
    )
}

export default StateManager(ModalWrapper) as React.ElementType

const GatherValuesAndSubmit = (props: { onHide: () => void, translation: string }) => {
    const { /*values,*/ submitForm } = useFormikContext()

    const handleSubmit = (e: React.FormEvent<HTMLButtonElement>) => {
        e.preventDefault()
        submitForm().catch(e => console.log(e.message))
    }

    return (
        <Modal.Footer>
            <Button
                variant="contained"
                color="primary"
                className="modal-close-button"
                onClick={props.onHide}
            >
                Sulje ikkuna
            </Button>
            <Button
                variant="contained"
                color="secondary"
                className="modal-add-button"
                onClick={handleSubmit}
                type="submit"
            >
                {`Lisää ${props.translation}`}
            </Button>
        </Modal.Footer>
    )
}

