import React from 'react'
import StateManager from './statemanager/StateManager'

type CandidateProps = {
    addVote: ContextType['addVote'];
    candidate: CandidateEntry;
}

const Candidate = ({ addVote, candidate }: CandidateProps) => {

    const handleVote = async (e: React.MouseEvent) => {
        e.preventDefault()
        addVote(candidate)
    }

    return (
        <div className="candidate-wrapper">
            { candidate ?
                <>
                    <h3>{candidate['CandidateName']}</h3>
                    <h6>Ikä: {candidate['CandidateAge']}</h6>
                    <section onClick={handleVote}>{candidate['CandidateNumber']}</section>
                    <h5>Äänet: {candidate['CandidateVotes']}</h5>
                </>
                : null
            }
        </div>
    )
}

export default StateManager(Candidate) as React.ElementType
