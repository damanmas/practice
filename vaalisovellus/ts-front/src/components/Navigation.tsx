import React, { useCallback, useState } from 'react'
import { Link } from 'react-router-dom'
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap'
import ModalWrapper from './modal/ModalWrapper'
import StateManager from './statemanager/StateManager'

type NavigationProps = {
    state: ContextState;
}

const Navigation: React.FC<NavigationProps> = ({ state }) => {
    const [electionModal, showElectionModal] = useState(false)

    const electionLinks = useCallback(() => {
        if(state.elections.list) {
            return state.elections.list.map((entry: ElectionEntry, i: number) => {
                return (
                    <NavDropdown.Item
                        key={i}
                        as={Link}
                        to={({ pathname: `/election/${entry.ElectionName}`, query: entry.ElectionId } as any)}
                    >
                        {entry.ElectionName}
                    </NavDropdown.Item>
                )
            })
        }
    }, [state.elections.list])

    return (
        <Container className="navbar-container">
            <Navbar collapseOnSelect expand="lg" className="navbar-wrapper">
                <Navbar.Brand>
                    <h2>Vaalisovellus</h2>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="nav-links">
                        <Nav.Item>
                            <Nav.Link as={Link} to="/">Etusivu</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link as={Link} to="/election-chart">Kuvaaja</Nav.Link>
                        </Nav.Item>
                        <NavDropdown title="Aktiiviset vaalit" id="basic-nav-dropdown">
                            { electionLinks() }
                        </NavDropdown>
                        <Nav.Item>
                            <Nav.Link as={Link} to="#" onClick={() => showElectionModal(true)}>Lisää vaali</Nav.Link>
                        </Nav.Item>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
            <ModalWrapper
                show={electionModal}
                onHide={() => showElectionModal(false)}
                current="election"
                electionid=""
            />
        </Container>
    )
}

export default StateManager(Navigation) as React.ElementType
