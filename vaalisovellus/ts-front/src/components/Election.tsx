import React, { useCallback, useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import { Button } from '@material-ui/core'
import { Row, Col, Container } from 'react-bootstrap'
import ModalWrapper from './modal/ModalWrapper'
import Candidate from './Candidate'
import StateManager from './statemanager/StateManager'

interface ElectionProps {
    state: ContextState
    setCurrentElection: ContextType['setCurrentElection'];
    getCandidates: ContextType['getCandidates'];
}

const Election: React.FC<ElectionProps> = ({ setCurrentElection, getCandidates, state }) => {
    const [candidateModal, showCandidateModal] = useState(false)
    const [electionName, setElectionName] = useState('')
    const [currentCandidates, setCurrentCandidates] = useState<CandidateEntry[] | null>(null)
    const { query, pathname }: any = useLocation() //as unknown as { query: string, pathname: string }
    const { elections, candidates } = state

    useEffect(() => {
        if(query) setCurrentElection(query)
    }, [query])

    useEffect(() => {
        if(elections.current) getCandidates(elections.current['ElectionId'])
    }, [elections.current])

    useEffect(() => {
        if(candidates.list) setCurrentCandidates(candidates.list)
    }, [candidates.list])

    useEffect(() => {
        setElectionName(pathname.slice(pathname.lastIndexOf('/') + 1))
    }, [pathname])

    const renderElectionData = useCallback(() => {
        if(currentCandidates && elections.current && elections.current['ElectionName'] === electionName) {
            return currentCandidates.map((candidate, i) => {
                return <Candidate key={i} candidate={candidate} />
            })
        }
        return <h4>Vaalitietoja ei löytynyt</h4>
    }, [currentCandidates])

    return (
        <Container className="h-100 w-100" fluid>
            <Button
                variant="contained"
                color="primary"
                className="add-candidate-button"
                onClick={() => showCandidateModal(true)}
            >
                Lisää uusi ehdokas
            </Button>
            <Row>
                <Col lg={12}>
                    <div className="centering">
                        <h1>{electionName}</h1>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <div className="candidate-container">
                        { renderElectionData() }
                    </div>
                </Col>
            </Row>
            <ModalWrapper
                show={candidateModal}
                onHide={() => showCandidateModal(false)}
                current="candidate"
                electionid={`/${query ? query : elections.current ? elections.current['ElectionId'] : ''}`}
            />
        </Container>
    )
}

export default StateManager(Election) as React.ElementType
