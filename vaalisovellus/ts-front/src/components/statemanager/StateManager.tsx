import { ReactNode } from 'react'
import { Consumer } from './StateContext'

const StateManager = (Component: any): ReactNode => {
	const HOC = (props: any) => {
		return (
			<Consumer>
				{ state => <Component {...props} {...state} /> }
			</Consumer>
		)
	}
	return HOC
}

export default StateManager