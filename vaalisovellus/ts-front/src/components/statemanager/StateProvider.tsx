import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Provider } from './StateContext'

const StateProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
    const [state, setState] = useState<ContextState>({
        elections: {
            list: [],
            current: null,
        },
        candidates: {
            list: []
        }
    })

    useEffect(() => {
        if(typeof window !== 'undefined') {
            if(localStorage.getItem('elections')) {
                setState(JSON.parse(localStorage.getItem('elections') || '{}'))
            }
        }
    }, [])

    useEffect(() => {
        if(typeof window !== 'undefined') {
            localStorage.setItem('elections', JSON.stringify(state))
        }
    }, [state])

    const addItemToArr = (parent: string, child: string, data: ElectionEntry | CandidateEntry) => {
		setState(prev => ({
			...prev,
			[parent]: {
				...prev[parent],
				[child]: prev[parent][child].concat(data)
			}
		}))
	}

	const addToStateProvider = (parent: string, child: string, data: ElectionEntry | ElectionEntry[] | CandidateEntry[]) => {
		setState(prev => ({
			...prev,
			[parent]: {
				...prev[parent],
				[child]: data
			}
		}))
	}

	const setCurrentElection = (query: string) => {
		const current = state.elections.list.filter(item => query === item.ElectionId)
		addToStateProvider('elections', 'current', current[0])
	}

	const getElections = async () => {
		try {
			const res = await axios.get(`${process.env.REACT_APP_SERVER_URL}/api/election/all`,
				{
					method: 'GET',
					mode: 'cors',
					headers: {
						'Content-type': 'application/json'
					}
				})
			const resolved: ElectionEntry[] = await res.data
			addToStateProvider('elections', 'list', resolved)
		} catch (e) {
			console.log('getElections error:',e)
		}
	}

	const getCandidates = async (id: string) => {
		try {
			const res = await axios.get(`${process.env.REACT_APP_SERVER_URL}/api/candidate/${id}`,
				{
					method: 'GET',
					mode: 'cors',
					headers: { 'Content-type': 'application/json' }
				})
			const resolved: CandidateEntry[] = await res.data
			addToStateProvider('candidates', 'list', resolved)
		} catch (e) {
			console.log('getCandidates error:',e)
		}
	}

	const addVote = async (candidate: CandidateEntry) => {
		if(window.confirm(`Haluatko varmasti äänestää ${candidate.CandidateName}? Äänestys mahdollista kerran vaalia kohden.`)) {
			try {
				const modified = {
					...candidate,
					CandidateVotes: candidate.CandidateVotes + 1
				}
				const res = await axios.put(
					`${process.env.REACT_APP_SERVER_URL}/api/candidate/${candidate.CandidateId}`,
					modified,
					{
						method: 'PUT',
						mode: 'cors',
						headers: {
							'Content-type': 'application/json',
							'type': 'voted'
						}
					})

				if(res.status === 200) {
					const resolved: CandidateEntry = await res.data
					const updatedList: CandidateEntry[] = state.candidates.list.map(candidate => {
						return candidate.CandidateId === resolved.CandidateId ? resolved : candidate
					})
					addToStateProvider('candidates', 'list', updatedList)
					getElections().catch(e => console.log(e.message))
				}
			} catch (e) {
				if(e.response.status === 403) {
					alert('Olet jo äänestänyt näissä vaaleissa.')
				} else {
					console.log('getElection error:',e)
				}
			}
		}
	}

	const AppContext: ContextType = {
		state: state,
		getElections,
		getCandidates,
		setCurrentElection,
		addItemToArr,
		addVote
	}

    return (
        <Provider value={AppContext}>
            {children}
        </Provider>
    )

}

export default StateProvider