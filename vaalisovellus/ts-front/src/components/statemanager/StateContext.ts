import React from 'react'

const defaultState = {
    elections: {
        list: [],
        current: null,
    },
    candidates: {
        list: []
    }
}

const contextDefaultValues: ContextType = {
    state: defaultState,
    getElections: () => undefined,
    getCandidates: () => undefined,
    setCurrentElection: () => undefined,
    addItemToArr: () =>  undefined,
    addVote: () => undefined,
}

const StateContext = React.createContext<ContextType>(contextDefaultValues)

export const Provider = StateContext.Provider

export const Consumer = StateContext.Consumer
