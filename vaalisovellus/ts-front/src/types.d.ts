
enum ElectionType {
    Eduskuntavaali = 'eduskuntavaali',
    Kunnallisvaali = 'kunnallisvaali',
    Presidentinvaali = 'presidentinvaali',
}

interface CandidateEntry {
    CandidateId: string;
    CandidateName: string;
    CandidateAge: number;
    CandidateVotes: number;
    CandidateNumber: number;
    Election_FK: number;
}

interface ElectionEntry {
    ElectionId: string;
    ElectionName: string;
    ElectionYear: number;
    ElectionType: ElectionType;
}

interface IObjectKeys {
    [key: string]: any;
}

interface Elections {
    list: ElectionEntry[],
    current: ElectionEntry | null,
}

interface Candidates {
    list: CandidateEntry[];
}

interface ContextState extends IObjectKeys {
    elections: Elections;
    candidates: Candidates;
}

interface ContextType {
    state: ContextState,
    getElections: () => void,
    getCandidates: (id: string) => void,
    setCurrentElection: (query: string) => void,
    addItemToArr: (parent: string, child: string, data: ElectionEntry | CandidateEntry) =>  void,
    addVote: (candidate: CandidateEntry) => void,
}

type GenericComponent<Props extends {}> =
    (props: Props, context?: any) => GenericComponent<any> | null | JSX.Element
