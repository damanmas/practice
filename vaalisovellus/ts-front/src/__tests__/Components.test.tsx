import { render, screen } from '@testing-library/react'
//import { shallow } from 'enzyme'
import { prettyDOM } from '@testing-library/dom'
import Home from '../components/Home'
//import Navigation from '../components/Navigation'
import ModalWrapper from '../components/modal/ModalWrapper'

describe('Components', () => {

  test('renders Home component v.1', () => {
    const component = render(<Home />)
    //component.debug()
    expect(component.container).toHaveTextContent('Tervetuloa vaalisovellukseen')
    //let h5 = component.container.querySelectorAll('h5')
    //console.log(prettyDOM(h5))
    //const mockHandler = jest.fn()
  })

  test('renders Home component v.2', () => {
    render(<Home />)
    const linkElement = screen.getByText(/Tervetuloa vaalisovellukseen/i)
    expect(linkElement).toBeInTheDocument()
  })

  /*test('renders Navigation component', () => {
    //shallow works with a locally assigned component
    const wrapper = shallow((<Navigation />))
    expect(wrapper.contains(<h2>Vaalisovellus</h2>)).toBeTruthy()
  })*/

  test('<ModalWrapper /> adds correct fields', () => {
    //const createCandidate = jest.fn()
    const component = render(
        <ModalWrapper show={true} onHide={() => {}} current="candidate" electionid="1" />
    )
    const input = component.container.querySelector('input')
    console.log(prettyDOM(input as HTMLInputElement))
    //const input = component.container.querySelectorAll('input')
    //const form = component.container.querySelector('form')
    //expect(input).toHaveLength(2)
    /*fireEvent.change(input, {
      target: { value: 'testing of forms could be easier' }
    })
    fireEvent.submit(form)
    expect(createNote.mock.calls[0][0].content).toBe('testing of forms could be easier' )*/
  })


})
