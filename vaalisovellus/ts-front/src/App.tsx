import React, { useEffect } from 'react'
import { Container } from 'react-bootstrap'
//import { GenericComponent } from './index.d'
import Navigation from './components/Navigation'
import Routes from './components/Routes'
import StateManager from './components/statemanager/StateManager'

interface AppProps {
    state: ContextState;
    getElections: ContextType['getElections'];
    //id?: string | undefined;
    //className?: string | undefined;
}

const App: React.FC<AppProps> = ({ state, getElections }) => {

    useEffect(() => {
        if(!(state.elections.list.length > 0)) getElections()
    }, [])

    return (
        <Container className="app-container" fluid>
            <header id="header">
                <Navigation />
            </header>
            <article id="content">
                <Routes />
            </article>
            <footer id="footer">
            </footer>
        </Container>
    )
}

export default StateManager(App) as React.ElementType
