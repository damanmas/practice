import React, { useEffect, useState } from 'react'
import StateContext from './StateContext'
import axios from 'axios'
axios.defaults.withCredentials = true

const defaultState = {
	elections: {
		list: [],
		current: null,
	},
	candidates: {
		list: []
	}
}

export default function StateProvider(props) {
    const [state, setState] = useState(defaultState)

    useEffect(() => {
        if(typeof window !== 'undefined') {
            if(localStorage.getItem('elections')) {
                setState(JSON.parse(localStorage.getItem('elections')))
            }
        }
    }, [])

    useEffect(() => {
        if(typeof window !== 'undefined') {
            localStorage.setItem('elections', JSON.stringify(state))
        }
    }, [state])

    const addItemToArr = (parent, child, data) => {
        setState(prev => ({
            ...prev,
            [parent]: {
                ...prev[parent],
                [child]: prev[parent][child].concat(data)
            }
        }))
    }

    const addToStateProvider = (parent, child, data) => {
        setState(prev => ({
            ...prev,
            [parent]: {
                ...prev[parent],
                [child]: data
            }
        }))
    }

    const setCurrentElection = (query) => {
        const found = state.elections.list.filter(item => query === item._id)
        addToStateProvider('elections', 'current', found[0])
    }

    const getElections = async () => {
        try {
            const res = await axios.get(`${process.env.REACT_APP_SERVER_URL}/api/election/all`,
                {
                    method: 'GET',
                    mode: 'cors',
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            const resolved = await res.data
            addToStateProvider('elections', 'list', resolved)
        } catch (e) {
            console.log('getElections error:',e)
        }
    }

    const getCandidates = async (id) => {
        try {
            const res = await axios.get(`${process.env.REACT_APP_SERVER_URL}/api/candidate/${id}`,
                {
                    method: 'GET',
                    mode: 'cors',
                    headers: { 'Content-type': 'application/json' }
                })
            const resolved = await res.data
            addToStateProvider('candidates', 'list', resolved)
        } catch (e) {
            console.log('getCandidates error:',e)
        }
    }

    const addVote = async (candidate) => {
        if(window.confirm(`Haluatko varmasti äänestää ${candidate['name']}? Äänestys mahdollista kerran vaalia kohden.`)) {
            try {
                let modified = {
                    ...candidate,
                    votes: candidate.votes + 1
                }
                const res = await axios.put(
                    `${process.env.REACT_APP_SERVER_URL}/api/candidate/${candidate['_id']}`,
                    modified,
                    {
                        method: 'PUT',
                        mode: 'cors',
                        headers: {
                            'Content-type': 'application/json',
                            'type': 'voted'
                        }
                    })

                if(res.status === 200) {
                    const resolved = await res.data
                    //const found = state.elections.list.filter(item => query === item._id)
                    const updatedList = state.candidates.list.map(candidate => {
                        return candidate._id === resolved._id ? resolved : candidate
                    })
                    addToStateProvider('candidates', 'list', updatedList)
                    getElections().catch(e => console.log(e.message))
                }
            } catch (e) {
                if(e.response.status === 403) {
                    alert('Olet jo äänestänyt näissä vaaleissa.')
                } else {
                    console.log('getElection error:',e)
                }
            }
        }
    }

    return(
        <StateContext.Provider value={{
            state: state,
            getElections: getElections,
            getCandidates: getCandidates,
            setCurrentElection: setCurrentElection,
            addItemToArr: addItemToArr,
            addVote: addVote
        }}>
            {props.children}
        </StateContext.Provider>
    )
}