import { Route, Switch } from 'react-router-dom'
import Election from './Election'
import ElectionChart from './ElectionChart'
import Home from './Home'

const Routes = () => {
    return (
        <Switch>
            <Route exact path="/" component={() => {
                return <Home />
            }}/>
            <Route exact path="/election/:name" component={Election} />
            <Route exact path="/election-chart" component={ElectionChart} />
        </Switch>
    )
}

export default Routes
