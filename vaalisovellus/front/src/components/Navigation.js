import { useCallback, useState } from 'react'
import { Link } from 'react-router-dom'
import StateManager from '../statemanager/StateManager'
import TailwindModalWrapper from './tailwind/TailwindModalWrapper'

const Navigation = (props) => {
    const [show, showModal] = useState(false)

    const electionLinks = useCallback(() => {
        if(props.state.elections.list) {
            return props.state.elections.list.map((e, i) => {
                return (
                    <Link
                        key={i}
                        href={{ pathname: `/election/${e.ElectionName}`, query: e.ElectionId }}>
                        {e.ElectionName}
                    </Link>
                )
            })
        }
    }, [props.state.elections.list])

    const handleModal = (e) => {
        e.preventDefault()
        let changed = !show
        showModal(changed)
    }

    return (
        <>
            <div className="container flex justify-center items-center">
                <div className="flex flex-row justify-between">
                    <div className="">
                        <h2>Vaalisovellus</h2>
                    </div>
                    <div className="flex flex-row lg:space-x-14 md:space-x-6">
                        <div>
                            <Link to="/">Etusivu</Link>
                        </div>
                        <div>
                            <Link to="/election-chart">Kuvaaja</Link>
                        </div>
                        <div title="Aktiiviset vaalit" id="basic-nav-dropdown">
                            <Link to="#">Aktiiviset vaalit</Link>
                            { electionLinks() }
                        </div>
                        <div>
                            <a href="#" onClick={handleModal} id="openElectionModal">Lisää vaali</a>
                        </div>
                        <div>
                            <a href="#" onClick={props.resetMemory}>Nollaa</a>
                        </div>
                    </div>
                </div>
            </div>

            <TailwindModalWrapper current="election" onHide={handleModal} show={show} />
        </>
    )
}

export default StateManager(Navigation)
