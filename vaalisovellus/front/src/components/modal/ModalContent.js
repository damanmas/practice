import {FormControl, FormGroup, InputLabel, MenuItem, Select, TextField} from "@material-ui/core";
import {Field} from "formik";

export const ElectionModalContent = () => {
    return (
        <>
            <div>
                <h4>Vaalin nimi</h4>
                <FormGroup>
                    <Field
                        as={TextField}
                        name="name"
                        label="Vaalin nimi"
                        placeholder="Vaalin nimi"
                        variant="outlined"
                        fullWidth
                    />
                </FormGroup>
            </div>
            <div>
                <h4>Vaalityyppi</h4>
                <FormControl fullWidth variant="outlined">
                    <InputLabel id={`search-`}>Vaalityyppi</InputLabel>
                    <Field name="type" as={Select} labelId={`search-`}>
                        <MenuItem value="Eduskuntavaali">Eduskuntavaali</MenuItem>
                        <MenuItem value="Kunnallisvaali">Kunnallisvaali</MenuItem>
                        <MenuItem value="Presidentinvaali">Presidentinvaali</MenuItem>
                    </Field>
                </FormControl>
            </div>
            <div>
                <h4>Järjestämisvuosi</h4>
                <FormGroup>
                    <Field
                        as={TextField}
                        name="year"
                        label="Järjestämisvuosi"
                        placeholder="Järjestämisvuosi"
                        variant="outlined"
                        fullWidth
                    />
                </FormGroup>
            </div>
        </>
    )
}

export const CandidateModalContent = () => {
    return (
        <>
            <div>
                <h4>Ehdokkaan nimi</h4>
                <FormGroup>
                    <Field
                        as={TextField}
                        name="name"
                        label="Ehdokkaan nimi"
                        placeholder="Ehdokkaan nimi"
                        variant="outlined"
                        fullWidth
                    />
                </FormGroup>
            </div>
            <div>
                <h4>Ehdokkaan ikä</h4>
                <FormGroup>
                    <Field
                        as={TextField}
                        name="age"
                        label="Ehdokkaan ikä"
                        placeholder="Ehdokkaan ikä"
                        variant="outlined"
                        fullWidth
                    />
                </FormGroup>
            </div>
        </>
    )
}
