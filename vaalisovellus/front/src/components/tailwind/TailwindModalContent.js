

export const TailwindElectionModalContent = () => {
    return (
        <div className="flex flex-col justify-center items-center h-100 py-12">
            <div className="w-8/12 max-w-md">
                <div className="grid grid-cols-1 gap-6">
                    <label className="block">
                        <span className="text-gray-700">Vaalin nimi</span>
                        <input
                            type="text"
                            className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                            placeholder=""
                        />
                    </label>
                    <label className="block">
                        <span className="text-gray-700">Vaalityyppi</span>
                        <select className="block w-full mt-1 rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
                            <option>Eduskuntavaali</option>
                            <option>Kunnallisvaali</option>
                            <option>Presidentinvaali</option>
                        </select>
                    </label>
                    <label className="block">
                        <span className="text-gray-700">Järjestämisvuosi</span>
                        <input
                            type="number"
                            className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                            placeholder=""
                        />
                    </label>
                </div>
            </div>
        </div>
    )
}

export const TailwindCandidateModalContent = () => {
    return (
        <div className="flex flex-col justify-center items-center h-100 py-12">
            <div className="w-8/12 max-w-md">
                <div className="grid grid-cols-1 gap-6">
                    <label className="block">
                        <span className="text-gray-700">Vaalin nimi</span>
                        <input
                            type="text"
                            className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                            placeholder=""
                        />
                    </label>
                    <label className="block">
                        <span className="text-gray-700">Vaalityyppi</span>
                        <select className="block w-full mt-1 rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
                            <option>Eduskuntavaali</option>
                            <option>Kunnallisvaali</option>
                            <option>Presidentinvaali</option>
                        </select>
                    </label>
                    <label className="block">
                        <span className="text-gray-700">Järjestämisvuosi</span>
                        <input
                            type="number"
                            className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                            placeholder=""
                        />
                    </label>
                </div>
            </div>
        </div>
    )
}