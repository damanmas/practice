import { TailwindElectionModalContent, TailwindCandidateModalContent } from './TailwindModalContent'

const TailwindModalWrapper = (props) => {
    return (
        <div className={`fixed top-20 ${props.show ? 'flex' : 'hidden'} flex-col justify-start items-center rounded border bg-gray-200 antialiased w-4/12 h-auto z-10`}>
            <div className="flex flex-col w-full h-full shadow-xl">
                <div className="flex flex-row justify-between p-6 bg-white border-b border-gray-200 rounded-tl rounded-tr">
                    <p className="font-semibold text-gray-800">Election</p>
                    <svg onClick={props.onHide}
                         className="w-5 h-6 cursor-pointer"
                         fill="none"
                         stroke="currentColor"
                         viewBox="0 0 24 24"
                         xmlns="http://www.w3.org/2000/svg">
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            d="M6 18L18 6M6 6l12 12"
                        />
                    </svg>
                </div>
                <>
                    { props.current && props.current === 'election' ?
                        <TailwindElectionModalContent />
                        :
                        <TailwindCandidateModalContent />
                    }
                </>
                <div className="flex flex-row justify-between p-6 bg-white border-b border-gray-200 rounded-bl rounded-br">
                    <button onClick={props.onHide} className="px-4 py-2 text-white font-semibold bg-blue-500 rounded">
                        Sulje
                    </button>
                    <button className="px-4 py-2 text-white font-semibold bg-green-500 rounded">
                        Lisää vaali
                    </button>
                </div>
            </div>
        </div>
    )
}

export default TailwindModalWrapper