
const Home = () => {
    return (
        <div>
            <h1 className="mb-5">Tervetuloa vaalisovellukseen</h1>
            <h5 className="mb-5">Navigoi linkkien kautta</h5>
            <h5 className="mb-5">Vaalisivu vaihtaa vain datan url:n muuttuessa</h5>
            <h5>Voit äänestää vain kerran / vaali painamalla ehdokasnumeroa</h5>
        </div>
    )
}

export default Home
