import { useCallback, useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import { Button } from '@material-ui/core'
import ModalWrapper from './modal/ModalWrapper'
import Candidate from './Candidate'
import StateManager from '../statemanager/StateManager'

const Election = (props) => {
    const [candidateModal, showCandidateModal] = useState(false)
    const [electionName, setElectionName] = useState('')
    const [currentCandidates, setCurrentCandidates] = useState(null)
    const { pathname } = useLocation()
    const { elections, candidates } = props.state

    useEffect(() => {
        if(candidates.list) setCurrentCandidates(candidates.list)
    }, [candidates.list])

    useEffect(() => {
        setElectionName(pathname.slice(pathname.lastIndexOf('/') + 1))
    }, [pathname])

    const renderElectionData = useCallback(() => {
        return currentCandidates && elections.current['ElectionName'] === electionName ?
            currentCandidates.map((candidate, i) => {
                return <Candidate key={i} candidate={candidate} />
            })
            : <h4>Vaalitietoja ei löytynyt</h4>
    }, [currentCandidates])

    return (
        <div>
            <Button
                variant="contained"
                color="primary"
                className="add-candidate-button"
                onClick={() => showCandidateModal(true)}>
                Lisää uusi ehdokas
            </Button>
            <div>
                <div>
                    <div className="centering">
                        <h1>{electionName}</h1>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div className="candidate-container">
                        { renderElectionData() }
                    </div>
                </div>
            </div>
            <ModalWrapper
                show={candidateModal}
                onHide={() => showCandidateModal(false)}
                current="candidate"
                electionid={`/${elections.current ? elections.current['ElectionId'] : ''}`}
            />
        </div>
    )
}

export default StateManager(Election)
