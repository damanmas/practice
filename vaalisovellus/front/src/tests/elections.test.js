import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, fireEvent } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { prettyDOM } from '@testing-library/dom'
import Election from '../components/Election'
import App from '../App'
import ModalWrapper from '../components/modal/ModalWrapper'

test('<Election /> new election can be created', () => {
    const createElection = jest.fn()

    const component = render(
        <App />
    )
    
    const link = component.container.querySelector('#openElectionModal')
    fireEvent.click(link)

    const component = render(
        <ModalWrapper />
    )

    component.debug()
    console.log(prettyDOM(component))

    const nameInput = component.container.querySelector('input[name="name"]')
    userEvent.type(nameInput, 'Mortal Kombat')
    const yearInput = component.container.querySelector('input[name="year"]')
    userEvent.type(yearInput, '2350')

    /*const form = component.container.querySelector('form')

    userEvent.type(input, 'testing of forms could be easier')
    fireEvent.submit(form)

    expect(createBlog.mock.calls).toHaveLength(1)
    expect(input).toHaveValue('testing of forms could be easier')
    expect(createBlog.mock.calls[0][0].content).toBe('testing of forms could be easier' )*/
})

/*
describe('<Togglable />', () => {
  let component

  beforeEach(() => {
    component = render(
      <Togglable buttonLabel="show...">
        <div className="testDiv" />
      </Togglable>
    )
  })

  test('renders its children', () => {
    expect(
      component.container.querySelector('.testDiv')
    ).toBeDefined()
  })

  test('at start the children are not displayed', () => {
    const div = component.container.querySelector('.togglableContent')

    expect(div).toHaveStyle('display: none')
  })

  test('after clicking the button, children are displayed', () => {
    const button = component.getByText('show...')
    fireEvent.click(button)

    const div = component.container.querySelector('.togglableContent')
    expect(div).not.toHaveStyle('display: none')
  })

})
*/

/*

  // method 1
  expect(component.container).toHaveTextContent(
    'Component testing is done with react-testing-library'
  )

  // method 2
  const element = component.getByText(
    'Component testing is done with react-testing-library'
  )
  expect(element).toBeDefined()

  // method 3
  const div = component.container.querySelector('.note')
  expect(div).toHaveTextContent(
    'Component testing is done with react-testing-library'
  )
})
*/

/*test('<Election /> updates parent state and calls onSubmit', () => {
    const createBlog = jest.fn()

    const component = render(
        <Election />
    )

    const input = component.container.querySelector('input[name="title"]')
    const form = component.container.querySelector('form')

    userEvent.type(input, 'testing of forms could be easier')
    fireEvent.submit(form)

    expect(createBlog.mock.calls).toHaveLength(1)
    expect(input).toHaveValue('testing of forms could be easier')
    //expect(createBlog.mock.calls[0][0].content).toBe('testing of forms could be easier' ) //didnt work, changed to userEvent
})

test('clicking the like-button calls event handler', async () => {
    const blog = {
        likes: 44,
        url: 'this.that.then',
        title: 'Potter Harry',
        author: 'JK Rowling',
        user: {
            username: 'masudd',
            name: 'Masud Däman',
            id: '5efda8e44f697a293889e555'
        },
        id: '5efdad632a8a474700d5c4f1'
    }

    const mockHandler = jest.fn()

    const component = render(
        <Blog key={blog.id} blog={blog} updateBlog={mockHandler} removeBlog={removeBlog} />
    )

    const viewButton = component.getByText(/view/i)
    fireEvent.click(viewButton)

    const likeButton = component.getByText(/like/i)
    fireEvent.click(likeButton)
    fireEvent.click(likeButton)

    //const div = component.container.querySelector('.blog')
    //console.log(prettyDOM(div))

    expect(mockHandler.mock.calls).toHaveLength(2)
})

test('correct parts of component is displayed', async () => {
    const blog = {
        title: 'Component testing is done with react-testing-library',
        author: 'author',
        url: 'url',
        likes: 0,
        id: 'id'
    }

    const component = render(
        <Blog key={blog.id} blog={blog} updateBlog={mockHandler} removeBlog={removeBlog} />
    )

    const button = component.getByText(/view/i)
    fireEvent.click(button)

    const div = component.container.querySelector('.blog')
    //console.log(prettyDOM(div))

    //expect(div).toEqual(expect.not.stringContaining('hide')) //dont work
})

test('renders content', () => {
    const blog = {
        title: 'Component testing is done with react-testing-library',
        author: 'author',
        url: 'url',
        likes: 0,
        id: 'id'
    }

    const component = render(
        <Blog key={blog.id} blog={blog} updateBlog={updateBlog} removeBlog={removeBlog}/>
    )

    //component.debug()

    //const li = component.container.querySelector('li')
    //console.log(prettyDOM(li))

    // tapa 1
    expect(component.container).toHaveTextContent('Component testing is done with react-testing-library')

    // tapa 2
    const element = component.getByText(/Component testing is done with react-testing-library/i)
    expect(element).toBeDefined()

    // tapa 3
    const div = component.container.querySelector('.blog')
    expect(div).toHaveTextContent('Component testing is done with react-testing-library')
})*/