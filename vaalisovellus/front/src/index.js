import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import App from './App'
import StateProvider from './statemanager/StateProvider'
import './styles/index.css'
require('dotenv').config()

ReactDOM.render(
    <React.StrictMode>
        <BrowserRouter>
            <StateProvider>
                <App />
            </StateProvider>
        </BrowserRouter>
    </React.StrictMode>,
    document.getElementById('root')
)
