Running of Elections-app v.1

1) Clone https://github.com/masudd/Futunio.git
2) Open each (front, ts-back) with a text-editor and run 'npm i' for each
3) Start ts-back using: 'npm run dev' (enough for a test run), start front using: 'npm start'
4) App should load on the default browser
