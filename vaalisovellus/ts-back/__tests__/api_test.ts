import { Election } from '../src/models/election'
import { Candidate } from '../src/models/candidate'
import { NewCandidateEntry } from '../src/types'
const mongoose = require('mongoose')
const supertest = require('supertest')
const app = require('../src/app')
const api = supertest(app)

describe('elections and candidates can be added', () => {

    beforeAll(async () => {
        await Election.deleteMany({})
        await Candidate.deleteMany({})
    })

    test('election can be added', async () => {
        const newElection = {
            name: 'Foo Bar',
            year: '2030',
            type: 'kunnallisvaali'
        }

        await api
            .post('/api/election')
            .send(newElection)
            .expect(200)
            .expect('Content-Type', /application\/json/)
    })

    test('candidate can be saved and referred to an election', async () => {
        let response = await api.get('/api/election/all')
        const id = response.body[0]['_id']

        const newCandidate = {
            name: 'John Doe',
            age: '18',
        }

        const anotherCandidate = {
            name: 'Jane Doe',
            age: '18',
        }

        await api
            .post(`/api/candidate/${id}`)
            .send(newCandidate)
            .expect(200)
            .expect('Content-Type', /application\/json/)

        await api
            .post(`/api/candidate/${id}`)
            .send(anotherCandidate)
            .expect(200)
            .expect('Content-Type', /application\/json/)

        response = await api.get('/api/election/all')

        expect(response.body[0].candidates).toHaveLength(2)
    })

    test('candidates are of the right type', async () => {
        const response = await api.get('/api/candidate/all')
        const candidates = response.body

        const checked = candidates.filter((candidate: NewCandidateEntry) => {
            return candidate.number === 0 && candidate.votes !== 0 && typeof candidate.name !== 'string'
        })

        expect(checked).toHaveLength(0)
    })

    afterAll(() => {
        mongoose.connection.close()
    })
})