const config = require('./src/utils/config')
const app = require('./src/app')
const http = require('http')

const server = http.createServer(app)

const PORT = config.port || 3002

server.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`)
})
