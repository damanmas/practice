import { Schema, model, Model } from 'mongoose'
const uniqueValidator = require('mongoose-unique-validator')
import { NewCandidateEntry } from '../types'

let candidateSchema = new Schema<NewCandidateEntry>({
    name: { type: String, required: true, minlength: 3, unique: true },
    age: { type: Number, required: true },
    number: { type: Number, required: true, unique: true },
    votes: { type: Number, required: true },
    election: {
        type: Schema.Types.ObjectId,
        ref: 'Election'
    }
}, {
    versionKey: false
})

candidateSchema.plugin(uniqueValidator)

export const Candidate: Model<NewCandidateEntry> = model<NewCandidateEntry>('Candidate', candidateSchema)
