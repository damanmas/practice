import { Schema, model, Model } from 'mongoose'
const uniqueValidator = require('mongoose-unique-validator')
import { NewElectionEntry } from '../types'

let electionSchema = new Schema<NewElectionEntry>({
    name: { type: String, required: true, minlength: 3, unique: true },
    year: { type: Number, required: true },
    type: { type: String },
    candidates: [{
        type: Schema.Types.ObjectId,
        ref: 'Candidate'
    }]
}, {
    versionKey: false
})

electionSchema.plugin(uniqueValidator)

export const Election: Model<NewElectionEntry> = model<NewElectionEntry>('Election', electionSchema)
