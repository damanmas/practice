import { ObjectID } from 'mongodb'

export interface ElectionEntry {
    _id: string;
    name: string;
    year: number;
    type: ElectionType;
    candidates: ObjectID[]
}

export interface CandidateEntry {
    _id: string;
    name: string;
    age: number;
    votes: number;
    number: number;
    election: ElectionEntry;
}

export type NewElectionEntry = Omit<ElectionEntry, '__v'>
export type NewCandidateEntry = Omit<CandidateEntry, '__v'>

export type ElectionBodyEntry = Omit<ElectionEntry, '__v'|'_id'|'candidates'>
export type CandidateBodyEntry = Omit<CandidateEntry, '__v'|'_id'|'election'>

export enum ElectionType {
    Eduskuntavaali = 'eduskuntavaali',
    Kunnallisvaali = 'kunnallisvaali',
    Presidentinvaali = 'presidentinvaali',
}


