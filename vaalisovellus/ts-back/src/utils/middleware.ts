
const checkActions = (req: any, res: any, next: any) => {
    if(req.session) {
        if(hasVoted(req)) {
            return next()
        } else {
            return res.status(403).json({message: 'forbidden. already performed this action once.'})
        }
    }
    next()
}

const hasVoted = (req: any): boolean => {
    if(req.session) {
        if(!req.session.voted) {
            req.session.voted = []
        }
        switch (req.headers.type) {
            case 'voted':
                if(req.session.voted.indexOf(req.body.election) !== -1) {
                    return false
                } else {
                    req.session.voted.push(req.body.election)
                    return true
                }
            default:
                return true
        }
    }
    return false
}

const requestLogger = (req: any, _res: any, next: any) => {
    console.log('Method:', req.method)
    console.log('Path:  ', req.path)
    console.log('Body:  ', req.body)
    console.log('---')
    return next()
}

const errorHandler = (error: any, _req: any, res: any, next: any): object | undefined => {
    console.log(error.message)
    if (error.name === 'CastError') {
        return res.status(400).send({ error: error.message })
    } else if (error.name === 'ValidationError') {
        return res.status(400).json({ error: error.message })
    }
    return next()
}

const randomNumber = (): number => {
    return Math.floor(Math.random() * 2500)
}

export default {
    requestLogger,
    errorHandler,
    randomNumber,
    checkActions
}
