const router = require('express').Router()
import { Candidate } from '../models/candidate'
import { Election } from '../models/election'
import { NewCandidateEntry } from '../types'
import { toNewCandidateEntry } from '../guards'

router.get('/candidate/:id', async (req: any, res: any): Promise<Array<NewCandidateEntry> | undefined> => {
    let { id } = req.params
    try {
        const findParams = id === 'all' ? {} : { election: id }
        const candidate = await Candidate.find(findParams)
        return res.status(200).json(candidate)
    } catch (e) {
        return res.status(e.response ? e.response.status : 409).json({message: 'conflict'})
    }
})

router.post('/candidate/:id', async (req: any, res: any): Promise<Array<NewCandidateEntry> | undefined> => {
    const candidateEntry = toNewCandidateEntry(req.body)
    const { id } = req.params
    try {
        const election = await Election.findOne({ _id: id })
        if(election) {
            const newCandidate = new Candidate({
                ...candidateEntry,
                election: election.id
            })
            const candidate = await newCandidate.save()
            election.candidates = election.candidates.length > 0 ? election.candidates.concat(candidate._id) : candidate._id
            election.save()
            return res.status(200).json(candidate)
        }
        return res.status(404).json({message: 'election not found'})
    } catch (e) {
        return res.status(e.response ? e.response.status : 409).json({message: 'conflict'})
    }
})

router.put('/candidate/:id', async (req: any, res: any): Promise<NewCandidateEntry | undefined> => {
    try {
        const updated = await Candidate.findByIdAndUpdate(req.params.id, req.body, {new: true})
        return res.status(200).json(updated)
    } catch (e) {
        return res.status(e.response ? e.response.status : 409).json({message: 'conflict'})
    }
})

export default { router }
