const router = require('express').Router()
import { Election } from '../models/election'
import { NewElectionEntry, ElectionEntry } from '../types'
import { toNewElectionEntry } from '../guards'

router.get('/election/:id', async (req: any, res: any): Promise<Array<NewElectionEntry> | undefined | number> => {
    let { id } = req.params
    try {
        const findParams = id === 'all' ? {} : { election: id }
        const elections: ElectionEntry[] = await Election.find(findParams).populate('candidates')
        return res.status(200).json(elections)
    } catch (e) {
        return res.status(e.response ? e.response.status : 409).json({message: 'conflict'})
    }
})

router.post('/election', async (req: any, res: any): Promise<NewElectionEntry | undefined | number> => {
    const electionEntry = toNewElectionEntry(req.body)
    try {
        const newElection = new Election({
            ...electionEntry,
            candidates: []
        })
        const election = await newElection.save()
        return res.status(200).json(election)
    } catch (e) {
        return res.status(e.response ? e.response.status : 409).json({message: 'conflict'})
    }
})

router.put('/election/:id', async (req: any, res: any): Promise<Array<NewElectionEntry> | undefined | number> => {
    try {
        const election = await Election.findByIdAndUpdate(req.params.id, req.body, {new: true})
        return res.status(200).json(election)
    } catch (e) {
        return res.status(e.response ? e.response.status : 409).json({message: 'conflict'})
    }
})

export default { router }
