import { ElectionBodyEntry, CandidateBodyEntry, ElectionType } from './types'
import middleware from './utils/middleware'

export const toNewElectionEntry = (body: any): ElectionBodyEntry => {
    const newEntry: ElectionBodyEntry = {
        name: parseString(body.name),
        year: parseNumber(body.year),
        type: parseElectionType(body.type),
    }
    return newEntry
}

export const toNewCandidateEntry = (body: any): CandidateBodyEntry => {
    const newEntry: CandidateBodyEntry = {
        name: parseString(body.name),
        age: parseNumber(body.age),
        number: middleware.randomNumber(),
        votes: 0,
    }
    return newEntry
}

const parseElectionType = (type: unknown): ElectionType => {
    if (!type || !isElectionType(type)) {
        throw new Error('Incorrect or missing election-type: ' + type)
    }
    return type
}

const isElectionType = (param: any): param is ElectionType => {
    return Object.values(ElectionType).includes(param.toLowerCase())
}

const parseNumber = (value: unknown): number => {
    if(!value || !isString(value) || !isNumber(value)) {
        throw new Error('Incorrect or missing number')
    }
    return value
}

const isNumber = (value: any): value is number => {
    return typeof(parseInt(value, 10)) === 'number'
}

const parseString = (param: unknown): string => {
    if(!param || !isString(param)) {
        throw new Error('Incorrect or missing string')
    }
    return param
}

const isString = (text: unknown): text is string => {
    return typeof text === 'string' || text instanceof String
}
