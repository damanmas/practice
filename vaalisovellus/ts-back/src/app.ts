//require('express-async-errors')
const express = require('express')
const app = express()
const cors = require('cors')
const mongoose = require('mongoose')
const session = require('express-session')
const MongoStore = require('connect-mongo')

import config from './utils/config'
import middleware from './utils/middleware'
import candidate from './routes/candidateRoutes'
import election from './routes/electionRoutes'

const mongoURL = config.MONGODB_URI

app.use(express.json())
app.use(cors({
    origin: 'http://localhost:3000',
    credentials: true
}))

mongoose.connect(mongoURL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('Connected to MongoDB'))
    .catch((err: any) => console.error('Error connecting to MongoDB:', err.message))

app.use(middleware.requestLogger)
app.use(middleware.errorHandler)

app.use(session({
    secret: 'elections-secret',
    store: MongoStore.create({ mongoUrl: mongoURL }),
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge: 60000, SameSite: 'none' }
}))

app.use('/api', [middleware.checkActions], [candidate.router, election.router])

module.exports = app
